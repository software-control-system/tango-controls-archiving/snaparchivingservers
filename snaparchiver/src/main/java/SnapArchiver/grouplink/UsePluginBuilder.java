/*
 * Synchrotron Soleil
 * 
 * File : TangoGroupForReadingAnyAttributesBuilder.java
 * 
 * Project : javaapi
 * 
 * Description :
 * 
 * Author : CLAISSE
 * 
 * Original : 17 janv. 07
 * 
 * Revision: Author:
 * Date: State:
 * 
 * Log: TangoGroupForReadingAnyAttributesBuilder.java,v
 */
/*
 * Created on 17 janv. 07
 * 
 * To change the template for this generated file go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
package SnapArchiver.grouplink;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.Target;
import fr.soleil.actiongroup.collectiveaction.components.tangowrapping.target.TargetFactory;
import fr.soleil.actiongroup.collectiveaction.onattributes.UsePlugin;
import fr.soleil.actiongroup.collectiveaction.onattributes.UsePluginImpl;
import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.PersistencePlugin;
import fr.soleil.actiongroup.collectiveaction.onattributes.plugin.context.PluginContext;
import fr.soleil.archiving.snap.api.persistence.SnapshotPersistenceManager;
import fr.soleil.archiving.snap.api.persistence.context.SnapshotPersistenceContext;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;

public class UsePluginBuilder {
    private final Logger logger = LoggerFactory.getLogger(UsePluginBuilder.class);
    private static final String TANGO_SEPARATOR = "/";
    private Map<String, Integer> attributeIds;

    private final int snapId;
    private final SnapshotPersistenceManager manager;

    public UsePluginBuilder(final int _snapId, final SnapshotPersistenceManager _manager) {
        snapId = _snapId;
        manager = _manager;
    }

    // public UsePlugin build ( List <INamedAttribute> attributeList ) throws
    // DevFailed
    public UsePlugin build(final List<SnapAttributeExtract> attributeList) throws DevFailed {
        attributeIds = new HashMap<String, Integer>(attributeList.size());
        final Map<String, Collection<String>> deviceToAttributes = new Hashtable<String, Collection<String>>();

        // SORT ATTRIBUTES BY DEVICE
        for (final SnapAttributeExtract attribute : attributeList) {
            final String completeName = attribute.getAttributeCompleteName();
            final String[] parsedName = parseName(completeName);
            final int attributeId = attribute.getAttId();
            attributeIds.put(completeName, attributeId);
            final String deviceName = parsedName[0];
            final String attributeName = parsedName[1];

            Collection<String> attributesForThisDevice = deviceToAttributes.get(deviceName);
            if (attributesForThisDevice == null) {
                attributesForThisDevice = new ArrayList<String>();
                deviceToAttributes.put(deviceName, attributesForThisDevice);
            }
            attributesForThisDevice.add(attributeName);
        }
        // traceDeviceToAttributes ( deviceToAttributes );

        // BUILD THE PROXIES AND ATTRIBUTES LIST
        final Target[] devices = new Target[deviceToAttributes.size()];

        final String[][] attributes = new String[deviceToAttributes.size()][];
        final Iterator<String> it2 = deviceToAttributes.keySet().iterator();
        int i = 0;

        while (it2.hasNext()) {
            final String nextDevice = it2.next();
            try {
                final DeviceProxy proxy = new DeviceProxy(nextDevice);
                devices[i] = TargetFactory.getTarget(proxy);
                final Collection<String> attributesForThisDevice = deviceToAttributes.get(nextDevice);
                logger.debug("snapshoting {}/{}", nextDevice, attributesForThisDevice);
                final Iterator<String> it3 = attributesForThisDevice.iterator();
                attributes[i] = new String[attributesForThisDevice.size()];
                int j = 0;
                while (it3.hasNext()) {
                    final String nextAttribute = it3.next();
                    attributes[i][j] = nextAttribute;
                    j++;
                }

                i++;
            } catch (final DevFailed e) {
                logger.error("error snapshoting device {}", nextDevice);
                logger.error("error msg = {}", DevFailedUtils.toString(e));
            } catch (final Exception e) {
                logger.error("error snapshoting device {}", nextDevice);
                logger.error("error", e);
            }

        }

        final UsePlugin ret = new UsePluginImpl(devices, attributes, new PersistencePlugin());
        logger.debug("launching snap ID {}", snapId);
        final SnapshotPersistenceContext persistenceContext = new SnapshotPersistenceContext(snapId, attributeIds);
        persistenceContext.setManager(manager);
        final PluginContext context = new PluginContext();
        context.setPersistenceContext(persistenceContext);
        ret.setPluginContext(context);

        return ret;
    }

    private String[] parseName(final String completeName) {
        final String[] ret = new String[2];
        final StringTokenizer st = new StringTokenizer(completeName, TANGO_SEPARATOR);
        final StringBuilder buff = new StringBuilder();

        buff.append(st.nextToken());
        buff.append(TANGO_SEPARATOR);
        buff.append(st.nextToken());
        buff.append(TANGO_SEPARATOR);
        buff.append(st.nextToken());

        ret[0] = buff.toString();
        ret[1] = st.nextToken();
        return ret;
    }

    /**
     * @return the attributeIds
     */
    public Map<String, Integer> getAttributeIds() {
        return attributeIds;
    }
}
