package SnapArchiver.grouplink;

public interface INamedAttribute {
	public String getCompleteName();
}
