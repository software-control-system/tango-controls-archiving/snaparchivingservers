package org.tango.server.snapextractor;

import fr.esrf.Tango.AttrDataFormat;
import fr.esrf.Tango.AttrWriteType;
import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoDs.TimedAttrData;
import fr.soleil.archiving.common.api.tools.DbData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.server.StateMachineBehavior;
import org.tango.server.attribute.AttributeConfiguration;
import org.tango.server.attribute.AttributePropertiesImpl;
import org.tango.server.attribute.AttributeValue;
import org.tango.server.attribute.IAttributeBehavior;
import org.tango.server.idl.TangoIDLUtil;

import java.lang.reflect.Array;

public class SnapDynamicAttribute implements IAttributeBehavior {

    final TimedAttrData[] timedAttrDatas;
    private final Logger logger = LoggerFactory.getLogger(SnapDynamicAttribute.class);
    private final AttributeConfiguration config = new AttributeConfiguration();
    private final AttributeValue readValue;

    public SnapDynamicAttribute(final String name, final DbData dbData) throws DevFailed {
        config.setName(name);
        config.setTangoType(dbData.getDataType(), AttrDataFormat.from_int(dbData.getDataFormat()));
        config.setWritable(AttrWriteType.READ);
        final AttributePropertiesImpl props = new AttributePropertiesImpl();
        props.setLabel(dbData.getName());
        config.setAttributeProperties(props);
        logger.debug("create attribute {}", config);
        timedAttrDatas = dbData.getDataAsTimedAttrData();

        final TimedAttrData timeData = timedAttrDatas[0];
        if (timeData.err != null && timeData.err.length > 0) {
            throw new DevFailed(timeData.err);
        }
        // try all types.
        Object value = timeData.bool_ptr;
        if (value == null) {
            value = timeData.lg64_ptr;
        }
        if (value == null) {
            value = timeData.lg_ptr;
        }
        if (value == null) {
            value = timeData.sh_ptr;
        }
        if (value == null) {
            value = timeData.str_ptr;
        }
        if (value == null) {
            value = timeData.db_ptr;
        }
        if (value == null) {
            value = timeData.fl_ptr;
        }
//        // get only first value
        if (config.getFormat().equals(AttrDataFormat.SCALAR)) {
            value = Array.get(value, 0);
        }
        readValue = new AttributeValue(value, timeData.qual, timeData.x, timeData.y,
                TangoIDLUtil.getTime(timeData.t_val));

        logger.debug("created attribute {} OK", name);
    }

    @Override
    public AttributeConfiguration getConfiguration() throws DevFailed {
        return config;
    }

    @Override
    public AttributeValue getValue() throws DevFailed {
        return readValue;
    }

    @Override
    public void setValue(final AttributeValue value) throws DevFailed {
        // not writable
    }

    @Override
    public StateMachineBehavior getStateMachine() throws DevFailed {
        return null;
    }

}
