package org.tango.server.snap.manager;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.DevVarLongStringArray;
import fr.esrf.TangoDs.Except;
import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.archiving.snap.api.manager.ISnapManager;
import fr.soleil.archiving.snap.api.manager.SnapManagerApi;
import fr.soleil.archiving.snap.api.manager.SnapManagerImpl;
import fr.soleil.archiving.snap.api.tools.SnapAttributeExtract;
import fr.soleil.archiving.snap.api.tools.SnapConst;
import fr.soleil.archiving.snap.api.tools.SnapContext;
import fr.soleil.archiving.snap.api.tools.Snapshot;
import fr.soleil.archiving.snap.api.tools.SnapshotingException;
import fr.soleil.database.connection.DataBaseParameters;
import fr.soleil.tango.clientapi.TangoCommand;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.DeviceState;
import org.tango.server.ServerManager;
import org.tango.server.annotation.Command;
import org.tango.server.annotation.Delete;
import org.tango.server.annotation.Device;
import org.tango.server.annotation.DeviceProperty;
import org.tango.server.annotation.Init;
import org.tango.server.annotation.State;
import org.tango.server.annotation.StateMachine;
import org.tango.server.annotation.Status;
import org.tango.utils.DevFailedUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * Class Description: This DServer provides the connections points and methods
 * to the SnapShot service.
 *
 * @author $Author: pierrejoseph $
 * @version $Revision: 1.22 $
 */
@Device
public class SnapManager {

    private static String VERSION;
    private final Logger logger = LoggerFactory.getLogger(SnapManager.class);
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();

    private final Map<Integer, Future<Integer>> snapshotResults = new HashMap<>();


    /**
     * User identifier (name) used to connect the database for snapshots. <br>
     * <b>Default value : </b> archiver
     */
    @DeviceProperty
    private String dbUser;
    /**
     * Password used to connect the database for snapshots. <br>
     * <b>Default value : </b> archiver
     */
    @DeviceProperty
    private String dbPassword;

    @DeviceProperty
    private String dbHost;
    @DeviceProperty
    private String dbName;
    @DeviceProperty
    private String dbSchema;
    @DeviceProperty(name = "maxDbPoolSize", description = "max database connections", defaultValue = "3")
    private short maxDbPoolSize = 3;
    @State
    private DeviceState state;
    @Status
    private String status;
    private ISnapManager manager;

    public static void main(final String[] args) {
        VERSION = ResourceBundle.getBundle("application").getString("project.version");
        ServerManager.getInstance().start(args, SnapManager.class);
    }

    public void setMaxDbPoolSize(short maxDbPoolSize) {
        this.maxDbPoolSize = maxDbPoolSize;
    }

    public void setDbUser(String dbUser) {
        this.dbUser = dbUser;
    }

    public void setDbPassword(String dbPassword) {
        this.dbPassword = dbPassword;
    }

    public void setDbHost(String dbHost) {
        this.dbHost = dbHost;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public void setDbSchema(String dbSchema) {
        this.dbSchema = dbSchema;
    }

    public DeviceState getState() {
        return state;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @org.tango.server.annotation.Attribute
    public String getVersion() {
        return VERSION;
    }

    /**
     * Initialize the device.
     */
    @Init
    public void init() throws DevFailed {
        try {
            // Snap Database connection
            DataBaseParameters parameters = new DataBaseParameters();
            parameters.setHost(dbHost);
            parameters.setUser(dbUser);
            parameters.setSchema(dbSchema);
            parameters.setPassword(dbPassword);
            parameters.setName(dbName);
            parameters.setMaxPoolSize(maxDbPoolSize);
            SnapManagerApi.initSnapConnection(parameters);
            state = DeviceState.ON;
            status = "Device is ready";
        } catch (ArchivingException e) {
            logger.error(e.toString(), e);
            throw e.toTangoException();
        }

    }


    /**
     * Execute command "CreateNewContext" on device. This command is used to
     * register a snapShot context. All informations concerning the snapshot
     * pattern (creation date, reason, description and, the <I>list of
     * attributes</I> that are included in the context) are passed through an
     * array (DEVVAR_STRINGARRAY).
     *
     * @param argin All the informations usefull to create a context ,Snapshot
     *              pattern).
     * @return The new assigned context ID
     */
    @Command(name = "CreateNewContext", inTypeDesc = "argin[0] = author name, argin[1] = context name, argin[2] = id (Not used, set it to 0)\n" +
            "argin[3] = creation date (yyyy-[m]m-[d]d), argin[4] = reason, argin[5] = description and, the list of" +
            " attributes that are included in the context", outTypeDesc = "The new assigned context ID")
    public int createNewContext(String[] argin) throws DevFailed {
        int argout=0;
        logger.info("Entering createNewContext with {}", Arrays.toString(argin));
        try {
            SnapContext snapContext = new SnapContext(argin);
            argout = SnapManagerApi.createContext(snapContext);
        } catch (SnapshotingException e) {
            logger.error(e.toString(), e);
            throw e.toTangoException();
        }catch (Throwable e){
            e.printStackTrace();
        }
        logger.info("Context created is {}", argout);
        return argout;
    }

    /**
     * Execute command "LaunchSnapShot" on device. This command is used to
     * trigger a snapShot. All informations concerning the snapshot will be
     * retrieved with the identifier parameter.
     *
     * @param argin The snapshot associated context's identifier.
     * @throws DevFailed
     */
    @Command(name = "LaunchSnapShot")
    @StateMachine(deniedStates = DeviceState.RUNNING)
    public void launchSnapShot(int argin) {
        logger.info("Entering llaunchSnapShot for context {}", argin);
        state = DeviceState.RUNNING;
        Future<Integer> snapshotResult = executorService.submit(new SnapshotTask(argin));
        snapshotResults.put(argin, snapshotResult);
    }

    @Command(name = "GetSnapShotResult")
    public int getSnapShotResult(int argin) throws DevFailed {
        int result = -1;
        try {
            result = snapshotResults.get(argin).get();
        } catch (InterruptedException e) {
            DevFailedUtils.throwDevFailed(e);
        } catch (ExecutionException e) {
            if (e.getCause() instanceof DevFailed) {
                throw (DevFailed) e.getCause();
            } else {
                DevFailedUtils.throwDevFailed(e);
            }
        }
        return result;
    }


    /**
     * Execute command "SetEquipmentsWithSnapshot" on device. This command is
     * used to set values to equipments.
     *
     * @param argin The snapshot from which equipments are set.
     */
    @Command(name = "SetEquipmentsWithSnapshot", inTypeDesc = "The snapshot from which equipments are set")
    public void setEquipmentsWithSnapshot(String[] argin) throws DevFailed {
        logger.info("Entering setEquipmentsWithSnapshot with {}", Arrays.toString(argin));
        try {
            Snapshot snapShot = new Snapshot(argin);
            // ---Add your Own code to control device here ---
            SnapManagerApi.TriggerSetEquipments(snapShot);
        } catch (SnapshotingException e) {
            e.printStackTrace();
            logger.error(e.toString(), e);
            throw e.toTangoException();
        } catch (Throwable e) {
            e.printStackTrace();
            throw DevFailedUtils.newDevFailed(e);
        }
        logger.info("Exiting setEquipmentsWithSnapshot");
    }

    /**
     * Execute command "SetEquipmentsWithCommand" on device. This command is
     * used to set snapshot attribute's values as command argument to
     * equipments.
     *
     * @param argin The command name and The snapshot id from which equipments are
     *              set.
     * @throws SnapshotingException
     */
    @Command(name = "SetEquipmentsWithCommand", inTypeDesc = "he command name and The snapshot id from which equipments are set")
    public String setEquipmentsWithCommand(String[] argin) throws DevFailed {
        try {
            String command_name = argin[0];
            int snap_id = Integer.parseInt(argin[2]);
            if (snap_id < 0) {
                throw DevFailedUtils.newDevFailed("Invalid SnapId");
            }
            // The attributes of the snapshot are gotten as they were saved in
            // the database.
            SnapAttributeExtract[] snapAttributeExtractArray;
            if (manager == null) {
                manager = new SnapManagerImpl();
            }
            snapAttributeExtractArray = manager.getSnap(snap_id);
            if (snapAttributeExtractArray == null || snapAttributeExtractArray.length == 0) {
                throw DevFailedUtils.newDevFailed("Invalid SnapId");
            }

            SnapAttributeExtract[] selectedAttributes = null;
            // If there is more than 3 arguments for this method,
            // we search if they are selected attributes for this snapshot (selected attributes).
            if (argin.length > 3) {
                List<SnapAttributeExtract> existingAttributes = new ArrayList<>();
                for (SnapAttributeExtract snapshotAttribute : snapAttributeExtractArray) {
                    for (int index = 3; index < argin.length; ++index) {
                        String element = argin[index];
                        if (element != null) {
                            if (element.equalsIgnoreCase(snapshotAttribute.getAttributeCompleteName())) {
                                existingAttributes.add(snapshotAttribute);
                                break;
                            }
                        }
                    }
                }
                if (existingAttributes.size() > 0) {
                    selectedAttributes = new SnapAttributeExtract[existingAttributes.size()];
                    selectedAttributes = existingAttributes.toArray(selectedAttributes);
                }
            } else {
                selectedAttributes = snapAttributeExtractArray;
            }

            // add attributes test: 1-different devices 2-same format
            if (validSnapshot(selectedAttributes)) {
                List<String> result = new ArrayList<>();
                boolean hasFailed = false;
                List<DevError> errorStack = new ArrayList<>();
                for (SnapAttributeExtract attributeSnap : selectedAttributes) {
                    try {
                        TangoCommand cmd = new TangoCommand(attributeSnap.getdeviceName(), command_name);
                        Object value = null;
                        if (SnapConst.STORED_READ_VALUE.equals(argin[1])) {
                            value = attributeSnap.getReadValue();
                            if (value == null) {
                                value = attributeSnap.getWriteValue();
                            }
                        } else if (SnapConst.STORED_WRITE_VALUE.equals(argin[1])) {
                            value = attributeSnap.getWriteValue();
                            if (value == null) {
                                value = attributeSnap.getReadValue();
                            }
                        }
                        logger.info(command_name + " with " + value + " on " + attributeSnap.getdeviceName());
                        cmd.execute(value);
                        if (!cmd.isArginVoid()) {
                            result.add(cmd.extractToString(","));
                        }
                    } catch (DevFailed e) {
                        hasFailed = true;
                        DevError[] err = e.errors;
                        for (DevError devError : err) {
                            errorStack.add(devError);
                        }
                    }
                }
                if (hasFailed) {
                    throw new DevFailed(errorStack.toArray(new DevError[errorStack.size()]));
                }
                return Arrays.toString(result.toArray(new String[result.size()]));
            }
        } catch (SnapshotingException | NumberFormatException e) {
            throw DevFailedUtils.newDevFailed(e);
        }
        return "";
    }

    /*
     * test if the attributes are from different devices and have the same
     * format
     */
    private boolean validSnapshot(SnapAttributeExtract[] selectedAttributes) throws DevFailed {
        int format = selectedAttributes[0].getDataFormat();
        int nbOccurs = numberOfOccurrences(selectedAttributes, selectedAttributes[0].getdeviceName());
        if (nbOccurs > 1) {
            Except.throw_exception("CONFIGURATION_ERROR", "A device cannot be selected more than one time",
                    "SnapManager.SetEquipmentsWithCommand");
            return false;
        }
        for (int i = 1; i < selectedAttributes.length; i++) {
            if (selectedAttributes[i].getDataFormat() != format) {
                Except.throw_exception(
                        "CONFIGURATION_ERROR", "Some attributes have incompatible format : "
                                + selectedAttributes[i].getDataFormat() + " and " + format,
                        "SnapManager.SetEquipmentsWithCommand");
                return false;
            } else if (numberOfOccurrences(selectedAttributes, selectedAttributes[i].getdeviceName()) > 1) {
                Except.throw_exception("CONFIGURATION_ERROR", "A device cannot be selected more than one time",
                        "SnapManager.SetEquipmentsWithCommand");
            }
        }
        return true;
    }

    /**
     * @param snapAttributeExtractArray
     * @param device
     * @return occurrence number of the input device name
     */
    private int numberOfOccurrences(SnapAttributeExtract[] snapAttributeExtractArray, String device) {
        int occ = 0;
        for (int i = 0; i < snapAttributeExtractArray.length; i++) {
            if (snapAttributeExtractArray[i].getdeviceName().equals(device)) {
                occ++;
            }
        }
        return occ;
    }


    /**
     * Execute command "SetEquipments" on device. This command is used to set
     * values to equipments.
     *
     * @param argin "SetEquipments arguments... <BR>
     *              <blockquote>
     *              <ul>
     *              <li><strong>First Case:</strong> Setpoint is done on all the snapshot attributes
     *              <ul>
     *              <li><var>argin</var>[0]<b> =</b> the snap identifier
     *              <li><var>argin</var>[1]<b> =</b> STORED_READ_VALUE (Setpoint with theirs read values) or
     *              STORED_WRITE_VALUE (Setpoint with theirs write values)<br>
     *              </ul>
     *              <li><strong>Second Case: </strong> Setpoint is done on a set of the snapshot attributes
     *              <ul>
     *              <li><var>argin</var>[0]<b> =</b> the snap identifier
     *              <li><var>argin</var>[1]<b> =</b> the number of attributes <br>
     *              Let us note <i>&quot;<var>index</var>&quot; </i>the last <var>index</var> used (for example, at this
     *              point, <i><var>index</var></i> = 2).
     *              <li><var>argin</var>[index]<b> =</b> NEW_VALUE or STORED_READ_VALUE or STORED_WRITE_VALUE
     *              <li><var>argin</var>[index+1]<b> =</b> the attribut name
     *              <li><var>argin</var>[index+2]<b> =</b> the value to set when NEW_VALUE is requested
     *              </ul>
     *              </blockquote>"
     */
    @Command(name = "SetEquipments", inTypeDesc = "SetEquipments arguments... <BR>\n" +
            "     *              <blockquote>\n" +
            "     *              <ul>\n" +
            "     *              <li><strong>First Case:</strong> Setpoint is done on all the snapshot attributes\n" +
            "     *              <ul>\n" +
            "     *              <li><var>argin</var>[0]<b> =</b> the snap identifier\n" +
            "     *              <li><var>argin</var>[1]<b> =</b> STORED_READ_VALUE (Setpoint with theirs read values) or\n" +
            "     *              STORED_WRITE_VALUE (Setpoint with theirs write values)<br>\n" +
            "     *              </ul>\n" +
            "     *              <li><strong>Second Case: </strong> Setpoint is done on a set of the snapshot attributes\n" +
            "     *              <ul>\n" +
            "     *              <li><var>argin</var>[0]<b> =</b> the snap identifier\n" +
            "     *              <li><var>argin</var>[1]<b> =</b> the number of attributes <br>\n" +
            "     *              Let us note <i>&quot;<var>index</var>&quot; </i>the last <var>index</var> used (for example, at this\n" +
            "     *              point, <i><var>index</var></i> = 2).\n" +
            "     *              <li><var>argin</var>[index]<b> =</b> NEW_VALUE or STORED_READ_VALUE or STORED_WRITE_VALUE\n" +
            "     *              <li><var>argin</var>[index+1]<b> =</b> the attribut name\n" +
            "     *              <li><var>argin</var>[index+2]<b> =</b> the value to set when NEW_VALUE is requested\n" +
            "     *              </ul>\n" +
            "     *              </blockquote>\"")
    public void setEquipments(String[] argin) throws DevFailed {
        logger.info("Entering setEquipments with {} ", Arrays.toString(argin));
        if (argin.length < 2) {
            throw DevFailedUtils.newDevFailed("Wrong number of parameters");
        } else {
            int snapId;
            try {
                // Retrieve the snapid value
                snapId = Integer.parseInt(argin[0]);
                if (manager == null) {
                    manager = new SnapManagerImpl();
                }
                // The attributes of the snapshot are gotten as they were saved
                // in the database.
                SnapAttributeExtract[] snapAttributeExtractArray = manager.getSnap(snapId);

                if (snapAttributeExtractArray == null || snapAttributeExtractArray.length == 0) {
                    throw DevFailedUtils.newDevFailed("Invalid SnapId");
                }
                Snapshot snapShot = Snapshot.getPartialSnapShot(argin, snapId, snapAttributeExtractArray);
                if (snapShot == null) {
                    return;
                }
                SnapManagerApi.TriggerSetEquipments(snapShot);
            } catch (SnapshotingException e) {
                logger.error(e.toString(), e);
                throw e.toTangoException();
            } catch (Throwable e) {
                throw DevFailedUtils.newDevFailed(e);
            }
        }
        logger.info("Exiting set_equipments()");
    }


    /**
     * Execute command "UpdateSnapComment" on device. This command updates the
     * comment of given snapshot
     *
     * @param argin 1) snapshot identifier 2) The new comment
     */
    @Command(name = "UpdateSnapComment", inTypeDesc = "1) snapshot identifier 2) The new comment")
    public void update_snap_comment(DevVarLongStringArray argin) throws DevFailed {
        logger.info("Entering update_snap_comment()");
        int id_snap = argin.lvalue[0];
        String comment = argin.svalue[0];
        try {
            SnapManagerApi.updateSnapComment(id_snap, comment);
        } catch (SnapshotingException e) {
            logger.error(e.toString(), e);
            throw e.toTangoException();
        }
        logger.info("Exiting update_snap_comment()");
    }

    @Command(name = "getSnapComment", inTypeDesc = "snap id")
    public String getSnapComment(int snapID) throws DevFailed {
        try {
            return Objects.toString(SnapManagerApi.getSnapComment(snapID));
        } catch (SnapshotingException e) {
            throw e.toTangoException();
        }
    }


    @Delete
    public void delete() {
        snapshotResults.clear();
    }

    private class SnapshotTask implements Callable<Integer> {
        private final int contextID;

        SnapshotTask(int contextID) {
            this.contextID = contextID;
        }

        @Override
        public Integer call() throws Exception {
            if (manager == null) {
                manager = new SnapManagerImpl();
            }
            SnapContext context = new SnapContext();
            Snapshot snapShot;
            context.setId(contextID);
            try {
                snapShot = manager.launchSnapshot(context);
            } catch (SnapshotingException e) {
                logger.error(e.toString(), e);
                throw e.toTangoException();
            } finally {
                state = DeviceState.ON;
            }
            logger.info("Exiting launch_snap_shot()");

            return snapShot.getId_snap();
        }
    }
}